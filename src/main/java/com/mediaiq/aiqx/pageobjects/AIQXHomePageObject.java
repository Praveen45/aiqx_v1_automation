package com.mediaiq.aiqx.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mediaiq.aiqx.helpers.BaseClass;

public class AIQXHomePageObject extends BaseClass {

	public AIQXHomePageObject(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	@FindBy(xpath = "//div[text() = 'DISCOVER']")
	public WebElement AIQXLandingPage_DiscoverTitle;

	@FindBy(xpath = "//img[@src = '/AiQ_logo-a931e74b8fb50b8ba905968c1e40b491.svg']")
	public WebElement AIQXLogo;

	@FindBy(xpath = "//div[@class = 'menu-icon pull-left']")
	public WebElement AIQMenuBar;

	@FindBy(xpath = "//span[text() = 'DISCOVER']")
	public WebElement AIQXDiscoverOption;

	@FindBy(xpath = "//span[text() = 'PLAN']")
	public WebElement AIQXPlanOption;

	@FindBy(xpath = "//span[text() = 'ACTIVATE']")
	public WebElement AIQXActivateOption;

	@FindBy(xpath = "//span[text() = 'SETTINGS']")
	public WebElement AIQXSettingOption;

	@FindBy(xpath = "//div[@id = 'root']/div/div[2]/button")
	public WebElement AIQXDriftButtton;

	@FindBy(xpath = "//span[contains(text() , 'Search for a company name and')]")
	public WebElement AIQXDiscoverText;

	@FindBy(xpath = "//div[@id = 'side-bar-content-wrapper']/div[1]/div/div[1]")
	public WebElement AIQXMenuBarCloseButton;

	@FindBy(xpath = "//div[@class = 'user-name']")
	public WebElement LoggedUserName;

	@FindBy(xpath = "//input[@placeholder = 'Search']")
	public WebElement SearchBox;

	@FindBy(xpath = "//button[@type = 'submit']")
	public WebElement SubmitButton;

	@FindBy(xpath = "//div[@class = 'double-bounce1']")
	public WebElement LoadingIcon;

	@FindBy(xpath = "//div[@class = 'query-text']")
	public WebElement SearchQueryText;

	@FindBy(xpath = "//input[@placeholder = '+ compare company']")
	public WebElement CompareCompanyBox;

	@FindBy(xpath = "//div[@id = 'traceCategory']")
	public WebElement TraceReportPanalView;

	// @FindBy(xpath =
	// "//a[@href = '/discover/all/crossDevicePath?q='"+brandname+"]")
	// public WebElement CrossDevicePath;

	public WebElement crossDevicePath(String brandname) {
		By crossDevicePath = By
				.xpath("//a[@href = '/discover/all/crossDevicePath?q="
						+ brandname + "']");
		return driver.findElement(crossDevicePath);
	}

	@FindBy(xpath = "//div[@id = 'crossDevicePath']/div[1]/div[text() = 'Cross Device: Top Converting Paths']")
	public WebElement CrossDevicePathReportTitle;

	@FindBy(xpath = "//div[@id = 'crossDevicePath']/div[1]/div/div[1]/div")
	public WebElement CrossDevicePathInformationIcon;

	@FindBy(xpath = "//button[contains(text(), 'Download report')]")
	public WebElement DownloadReportButton;

	@FindBy(xpath = "//button[text() = 'Add to RFP']")
	public WebElement AddToRFPButton;
	
	@FindBy(xpath = "//div[text() = 'Back']")
	public WebElement BackButton;

}
