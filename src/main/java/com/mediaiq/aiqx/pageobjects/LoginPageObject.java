package com.mediaiq.aiqx.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mediaiq.aiqx.helpers.BaseClass;

public class LoginPageObject extends BaseClass {

	public LoginPageObject(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	@FindBy(id = "username")
	public WebElement userName;

	@FindBy(id = "password")
	public WebElement password;

	@FindBy(xpath = "//button[text() = 'SIGN IN']")
	public WebElement signInButton;

	@FindBy(xpath = "//a[text() = 'SIGN UP']")
	public WebElement signup;
	
	@FindBy(xpath = "//a[text() = 'Google']")
	public WebElement signinwithgoogle;

	@FindBy(xpath = "//div[@class = 'user-options']")
	public WebElement UserLogoutOption;
	
	@FindBy(xpath = "//a[text() = 'Logout']")
	public WebElement LogoutButton;
	
	@FindBy(xpath = "//input[@type = 'email']")
	public WebElement googleemail;
	
	@FindBy(xpath = "//div[@id = 'password']/div[1]/div/div[1]/input")
	public WebElement googlepasssword;
	
	@FindBy(xpath = "//span[text() = 'Next'] ")
	public WebElement NextButton;
	
	
	
	@FindBy(name = "name")
	public WebElement aiqxaccessname;

	@FindBy(name = "email")
	public WebElement aiqxaccessemail;

	@FindBy(name = "organization")
	public WebElement aiqxaccessorganization;

	@FindBy(name = "jobTitle")
	public WebElement aiqxaccessjobtitle;

	@FindBy(name = "phoneNumber")
	public WebElement aiqxaccessphonenumber;

	@FindBy(xpath = "//input[@value = 'email']")
	public WebElement aiqxaccessemailoption;

	@FindBy(xpath = "//button[text() = 'Cancel']")
	public WebElement aiqxaccesscanceloption;

	@FindBy(xpath = "//div[@class = 'login-header']/h3")
	public WebElement aiqxacknowledgemessage;
	
	@FindBy(xpath = "//div[text() = 'Welcome to MediaIQ']")
	public WebElement aiqxwelcomemessage;

}
