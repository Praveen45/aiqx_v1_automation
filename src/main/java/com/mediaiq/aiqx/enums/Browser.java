package com.mediaiq.aiqx.enums;

public enum Browser {
	CHROME("CHROME"),
	FIREFOX("	FIREFOX"),
	INTERNET_EXPLORER("INTERNET_EXPLORER");
	
	String browser;
	
	Browser(String browser) {
		this.browser = browser;
	}

}
