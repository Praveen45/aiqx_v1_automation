package com.mediaiq.aiqx.step_definitions;

import java.net.MalformedURLException;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import com.mediaiq.aiqx.enums.Browser;
import com.mediaiq.aiqx.modules.AIQXHome;
import com.mediaiq.aiqx.modules.AIQXLogin;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class TestStepsAIQXHome {
	private WebDriver driver;
	private Browser browser;
	private static String brandName;

	public TestStepsAIQXHome() throws MalformedURLException {
		driver = Hooks.driver;
	}

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public Browser getBrowser() {
		return browser;
	}

	public void setBrowser(Browser browser) {
		this.browser = browser;
	}

	public static String brandName() {
		return brandName;

	}

	@Given("^I navigate to the aiqx URL$")
	public void i_navigate_to_the_aiqx_URL() throws Throwable {
		AIQXLogin.i_navigate_to_the_aiqx_URL();
	}

	@And("^I enter the Username as (.+) and  Password as (.+) and click on Sign in button$")
	public void i_enter_the_username_as_and_password_as_and_click_on_sign_in_button(
			String username, String password) throws Throwable {
		AIQXLogin
				.i_enter_the_username_as_and_password_as_and_click_on_sign_in_button(
						username, password);

	}

	@Then("^I verify the aiqx Landing Discover title and logged username as (.+)$")
	public void i_verify_the_aiqx_landing_discover_title_and_logged_username_as(
			String username) throws Throwable {
		AIQXHome.i_verify_the_aiqx_landing_discover_title_and_logged_username_as(username);
		;
	}

	@And("^Enter the  (.+) and  (.+) and click on Sign in button$")
	public void enter_the_and_and_click_on_sign_in_button(String username,
			String password) throws Throwable {
		AIQXLogin.enter_the_and_and_click_on_sign_in_button(username, password);
	}

	@Then("^Aiqx Landing Discover title and logged username as (.+)$")
	public void aiqx_landing_discover_title_and_logged_username_as(
			String username) throws Throwable {

		AIQXHome.aiqx_landing_discover_title_and_logged_username_as(username);

	}

	@Then("^Click on Logout$")
	public void click_on_Logout() throws Throwable {
		AIQXHome.click_on_Logout();
	}

	// AIQX landing Page Validation

	@Given("^I enter into the homepage of AIQX application with valid (.+) and (.+)$")
	public void i_enter_into_the_homepage_of_aiqx_application_with_valid_and(
			String username, String password) throws Throwable {
		AIQXHome.i_enter_into_the_homepage_of_aiqx_application_with_valid_and(
				username, password);
	}

	@And("^I verify that search box is auto focus$")
	public void i_verify_that_search_box_is_auto_focus() throws Throwable {
		AIQXHome.i_verify_that_search_box_is_auto_focus();
	}

	@And("^I check the menu icon bar is clickable$")
	public void i_check_the_menu_icon_bar_is_clickable() throws Throwable {
		AIQXHome.i_check_the_menu_icon_bar_is_clickable();
	}

	@And("^I verify the menu list items$")
	public void i_verify_the_menu_list_items() throws Throwable {
		AIQXHome.i_verify_the_menu_list_items();
	}

	// Report Landing screen
	@Given("^I enter the (.+) in search box of discover page$")
	public void i_enter_the_in_search_box_of_discover_page(String brandname)
			throws Throwable {
		brandName = brandname;
		AIQXHome.i_enter_the_in_search_box_of_discover_page(brandname);

	}

	@And("^I click on submit button$")
	public void i_click_on_submit_button() throws Throwable {
		AIQXHome.i_click_on_submit_button();
	}

	@And("^Loading icon should appear on the screen$")
	public void loading_icon_should_appear_on_the_screen() throws Throwable {
		AIQXHome.loading_icon_should_appear_on_the_screen();
	}

	@Then("^I should able to see the Report landing page with query text as (.+)$")
	public void i_should_able_to_see_the_report_landing_page_with_query_text_as(
			String brandname) throws Throwable {
		AIQXHome.i_should_able_to_see_the_report_landing_page_with_query_text_as(brandname);
	}

	@And("^Compare Company  text box should be enable$")
	public void compare_company_text_box_should_be_enable() throws Throwable {
		AIQXHome.compare_company_text_box_should_be_enable();
	}

	@And("^Search bar should not have auto focus in report landing screen$")
	public void search_bar_should_not_have_auto_focus_in_report_landing_screen()
			throws Throwable {
		AIQXHome.search_bar_should_not_have_auto_focus_in_report_landing_screen();
	}

	// Cross Device: Converison Path Report

	@Given("^I click on Cross Device converting path report for the searched (.+)$")
	public void i_click_on_cross_device_converting_path_report_for_the_searched(
			String brandname) throws Throwable {
		AIQXHome.i_click_on_cross_device_converting_path_report_for_the_searched(brandname);
	}

	@And("^I verify the report name$")
	public void i_verify_the_report_name() throws Throwable {
		AIQXHome.i_verify_the_report_name();
	}

	@And("^I click on information icon and verify the icon$")
	public void i_click_on_information_icon_and_verify_the_icon() throws Throwable {
		AIQXHome.i_click_on_information_icon_and_verify_the_icon();
	}

	@And("^I verify the DownloadReport and Add to RFP tabs are enbale$")
	public void i_verify_the_DownloadReport_and_Add_to_RFP_tabs_are_enbale() throws Throwable {
		AIQXHome.i_verify_the_DownloadReport_and_Add_to_RFP_tabs_are_enbale();
	}

	@Then("^I make a redshift connection$")
	public void i_make_a_redshift_connection() throws Throwable {
		AIQXHome.i_make_a_redshift_connection();
	}

	@And("^I validate the report query and verify the same in UI$")
	public void i_validate_the_report_query_and_verify_the_same_in_ui()
			throws Throwable {
		AIQXHome.i_validate_the_report_query_and_verify_the_same_in_ui();
	}

}
