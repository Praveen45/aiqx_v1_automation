package com.mediaiq.aiqx.step_definitions;

import java.net.MalformedURLException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.mediaiq.aiqx.enums.Browser;
import com.mediaiq.aiqx.helpers.BaseClass;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {
	public static WebDriver driver;
	public static Browser browser;
	private static String RedShiftDBUserName;
	private static String RedShiftDBPassword;
	private static String RedShiftDBURL = "jdbc:mysql://tradingcentre-staging-migrated.cmfydxrklw15.us-east-1.rds.amazonaws.com:3306/aiqx?nullNamePatternMatchesAll=true";
	

	@Before("@Start")
	/**
	 * Delete all cookies at the start of each scenario to avoid
	 * shared state between tests
	 */
	public void init() throws MalformedURLException {
		browser = browser.CHROME;

		switch (browser) {
		case CHROME:
			System.out.println("Called openBrowser");
			//System.setProperty("webdriver.chrome.driver",
					//"Drivers//chromedriver.exe");
			if(System.getProperty("os.name").contains("Windows")){
	    		//For windows
	    		System.setProperty("webdriver.chrome.driver", "Drivers//chromedriver.exe");
	    	}else{
	    		//For Linux
	    		System.setProperty("webdriver.chrome.driver", "Drivers//chromedriver");
	    	}
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--disable-extensions");
			options.addArguments("--disable-web-security");
			options.addArguments("--no-proxy-server");
			Map<String, Object> prefs = new HashMap<String, Object>();
		    prefs.put("credentials_enable_service", false);
		    prefs.put("profile.password_manager_enabled", false);
		    options.setExperimentalOption("prefs", prefs);
			driver = new ChromeDriver(options);
			/*driver = new ChromeDriver();*/
			break;
		case FIREFOX:
			driver = new FirefoxDriver();
			break;
		case INTERNET_EXPLORER:
			break;
		default:
			break;
		}
		openBrowser();
	}
	public void resizeBrowser() {
        Dimension d = new Dimension(1920, 1080);
        //Resize current window to the set dimension
        driver.manage().window().setSize(d);
   }

	public void openBrowser() throws MalformedURLException {
        resizeBrowser();
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);

	}
	@Before("@SetupRedShiftDB")
	// To establish database connection
	public static void setUpProduction() {
		String connectionUrl = RedShiftDBURL;
		RedShiftDBUserName = "manage_dbuser";
		RedShiftDBPassword = "Test123Manage";
		BaseClass.redshiftconnection = null;

		try {
			Class.forName("com.amazon.redshift.jdbc41.Driver");
			// Class.forName("com.amazon.redshift.jdbc4.Driver");
			System.out.println("Connecting to database");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		try {
			BaseClass.redshiftconnection = DriverManager.getConnection(
					connectionUrl, RedShiftDBUserName, RedShiftDBPassword);
			if (BaseClass.redshiftconnection != null) {
				System.out.println("Connected to the RedShift database...");
			} else {
				System.out
						.println("RedShift Database connection failed to  Environment");
			}
		} catch (SQLException sqlEx) {
			System.out.println("SQL Exception:" + sqlEx.getStackTrace());
		}
	}

	@After("@TearDownRedShiftDB")
	// To close database connection
	public static void tearDownProduction() {
		if (BaseClass.redshiftconnection != null) {
			try {
				System.out.println("Closing RedShift Database Connection...");
				BaseClass.redshiftconnection.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
	}

	
	@After("@NegativeTest")
	public void beforeScenario(Scenario scenario) {
		// System.out.println("In hooks");
		// System.out.println(scenario.getName());
		// System.out.println(scenario.getStatus());
	}
	@After
	/**
	 * Embed a screenshot in test report if test is marked as failed
	 */
	public void embedScreenshot(Scenario scenario) {

		if (scenario.isFailed()) {
			try {
				scenario.write("Current Page URL is " + driver.getCurrentUrl());
				// byte[] screenshot = getScreenshotAs(OutputType.BYTES);
				byte[] screenshot = ((TakesScreenshot) driver)
						.getScreenshotAs(OutputType.BYTES);
				scenario.embed(screenshot, "image/png");
			} catch (WebDriverException somePlatformsDontSupportScreenshots) {
				System.err.println(somePlatformsDontSupportScreenshots
						.getMessage());
			}

		}
	}
}
