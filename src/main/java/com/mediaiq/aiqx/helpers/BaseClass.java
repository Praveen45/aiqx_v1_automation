package com.mediaiq.aiqx.helpers;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import java.net.URL;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import java.net.HttpURLConnection;

import org.apache.commons.io.FileUtils;

import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public abstract class BaseClass {
	public static WebDriver driver;
	public static boolean bResult;
	public static String SA;

	public static Connection redshiftconnection;

	public BaseClass(WebDriver driver) {
		BaseClass.driver = driver;
		BaseClass.bResult = true;
	}

	public static void clear(WebElement element) {
		// element.clear();
		/*
		 * WebDriverWait wait = new WebDriverWait(driver,30); WebElement
		 */
	}

	public static String readWorkBook(String workBookName, String sheetName,
			int row, int column) throws FileNotFoundException, IOException {
		return DataHelper.readWorkBook(DataHelper.projectLocation,
				workBookName, sheetName, row, column);
	}

	/*
	 * public static void customSelect(WebElement selectBox, WebElement
	 * selectTextbox, String value) throws InterruptedException{
	 * selectBox.click(); selectTextbox.sendKeys(value); Thread.sleep(5000);
	 * selectTextbox.sendKeys(Keys.ENTER); selectTextbox.sendKeys(Keys.TAB);
	 * System.out.println("customSelect worked");
	 * 
	 * }
	 */
	public static String JS_DRAG_AND_DROP = "var src=arguments[0],tgt=arguments[1];var dataTransfer={dropEff"
			+ "ect:'',effectAllowed:'all',files:[],items:{},types:[],setData:f"
			+ "unction(format,data){this.items[format]=data;this.types.append("
			+ "format);},getData:function(format){return this.items[format];},"
			+ "clearData:function(format){}};var emit=function(event,target){v"
			+ "ar evt=document.createEvent('Event');evt.initEvent(event,true,f"
			+ "alse);evt.dataTransfer=dataTransfer;target.dispatchEvent(evt);}"
			+ ";emit('dragstart',src);emit('dragenter',tgt);emit('dragover',tg"
			+ "t);emit('drop',tgt);emit('dragend',src);";

	public static void paddingoption(WebElement editbox, WebElement Iconbox,
			WebElement deletebox, WebElement Sharebox,
			WebElement ShareInputControllink, WebElement OwnerIcon) {
		org.testng.Assert.assertTrue(OwnerIcon.isEnabled());
		org.testng.Assert.assertTrue(OwnerIcon.isEnabled());
		org.testng.Assert.assertTrue(Iconbox.isEnabled());
		org.testng.Assert.assertTrue(deletebox.isEnabled());
		org.testng.Assert.assertTrue(Sharebox.isEnabled());
		org.testng.Assert.assertTrue(ShareInputControllink.isEnabled());
		org.testng.Assert.assertTrue(OwnerIcon.isEnabled());

	}

	public static String CreateParentWindowHandle(WebDriver driver,
			String winHandleBefore) {

		winHandleBefore = driver.getWindowHandle();
		// System.out.println("Multiple Window");
		return winHandleBefore;
	}

	public static boolean isElementPresent(WebElement locatorKey) {
		try {
			locatorKey.isDisplayed();
			return true;
		} catch (org.openqa.selenium.NoSuchElementException e) {
			return false;
		}
	}

	public static void takeScreenShot() throws IOException {
		File scrFile = (File) ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);

		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		Calendar cal = Calendar.getInstance();
		System.out.println(dateFormat.format(cal.getTime()));

		String scrFilepath = scrFile.getAbsolutePath();
		System.out.println("scrFilepath: " + scrFilepath);

		File currentDirFile = new File("Screenshots");
		String path = currentDirFile.getAbsolutePath();
		System.out.println("path: " + path + "+++");

		System.out.println("****\n" + path + "\\screenshot"
				+ dateFormat.format(cal.getTime()) + ".png");

		FileUtils.copyFile(
				scrFile,
				new File(path + "\\screenshot"
						+ dateFormat.format(cal.getTime()) + ".png"));

	}

	// Code to fetch the similar advertiser for any brand
	// HTTP POST request
	public static void sendPost() throws Exception {

		String serveraddress = "http://52.90.101.136:5000/user_query_";
		// URL obj = new URL(se);
		// HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
		URL url = new URL(serveraddress);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();

		// add request header
		con.setRequestMethod("POST");

		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		// con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("Accept", "application/json");

		String urlParameters = "{\"query\":\"samsung\",\"country\":\"global\"}";

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(
				con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		// print result
		System.out.println(response.toString());
		// JsonObject jsonobj = new
		// JsonParser().parse(response.toString()).getAsJsonObject();
		JSONObject json = new JSONObject(response.toString());
		JSONArray jsonarray = json.getJSONArray("results");
		// JsonArray jsonarray = jsonobj.getAsJsonArray("results");
		String[] arr = new String[jsonarray.length()];
		for (int i = 0; i < jsonarray.length(); i++) {
			JSONObject obj = jsonarray.getJSONObject(i);
			arr[i] = String.valueOf(obj.getInt("advertiser_id"));

		}
		SA = String.join(",", arr);
		System.out.println("Final Similar Advertiser: " + SA);
	}

}
