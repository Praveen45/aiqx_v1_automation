package com.mediaiq.aiqx.helpers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class DataHelper {
	
	public static final String projectLocation = "D://Automation Project//AIQ_BI_V1//src//test//resources//testData";
	
	
	//Note the row & column value starts with (0,0) to exclude header use (1,0)
	public static String readWorkBook(String projectLocation, String workBookName, String sheetName, int row, int column)throws FileNotFoundException, IOException {
		String value = null;
		XSSFWorkbook workbook = new XSSFWorkbook(new FileInputStream(projectLocation + "//"+ workBookName));
		XSSFSheet sheet = workbook.getSheet(sheetName);
		Row currentRow = sheet.getRow(row);
		if (currentRow != null) {

			Cell currentCell = currentRow.getCell(column);
			switch (currentCell.getCellType()) {
			case Cell.CELL_TYPE_STRING:
				value = currentCell.getStringCellValue();
				break;
			case Cell.CELL_TYPE_NUMERIC:
				value = String.valueOf(currentCell.getNumericCellValue());
				break;
			}
		} else {
			System.out.println("The desired cell value does not exists");
		}
		return value;

	}
}
