package com.mediaiq.aiqx.modules;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import com.mediaiq.aiqx.helpers.BaseClass;
import com.mediaiq.aiqx.helpers.Log;
import com.mediaiq.aiqx.pageobjects.LoginPageObject;
import com.mediaiq.aiqx.step_definitions.Hooks;

public class AIQXLogin extends BaseClass {

	public static LoginPageObject loginPage = PageFactory.initElements(
			Hooks.driver, LoginPageObject.class);
	static WebDriverWait wait = new WebDriverWait(driver, 10);

	public AIQXLogin(WebDriver driver) {
		// TODO Auto-generated constructor stub

		super(driver);

	}

	public static void ExecuteOne(WebDriver driver) throws Exception {

		// login code via Valid Username & Password
		System.out.println("attempting to login");

		loginPage.userName.sendKeys("devuser");
		Log.info("Enter user name");

		loginPage.password.sendKeys("aiqxCIA2016");
		Log.info("Enter password");

		loginPage.signInButton.click();
		Log.info("Click action is performed on Sign in button");
		Reporter.log("SignIn Action is successfully performed");

	}

	// login code via Google
	public static void ExecuteTwo(WebDriver driver) throws Exception {

		System.out.println("attempting to login via Google");
		loginPage.signinwithgoogle.click();
		Log.info("Login via Google get click");

		loginPage.googleemail.sendKeys("praveen@mediaiqdigital.com");
		Log.info("Enter user name");

		loginPage.NextButton.click();
		Log.info("Next button gets click");

		loginPage.googlepasssword.sendKeys("Praveen1605@");
		Log.info("Enter password");

		loginPage.NextButton.click();
		Log.info("Next button gets click");
		Reporter.log("SignIn Action is successfully performed");

	}

	public static void i_navigate_to_the_aiqx_URL() throws Throwable {
		driver.navigate().to("https://staging.aiq.io");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

	}

	public static void i_enter_the_username_as_and_password_as_and_click_on_sign_in_button(
			String username, String password) throws Throwable {
		// login code via Valid Username & Password
		System.out.println("attempting to login");

		loginPage.userName.sendKeys(username);
		Log.info("Enter user name");

		loginPage.password.sendKeys(password);
		Log.info("Enter password");

		loginPage.signInButton.click();
		Log.info("Click action is performed on Sign in button");
		Reporter.log("SignIn Action is successfully performed");

	}

	public static void enter_the_and_and_click_on_sign_in_button(
			String username, String password) throws Throwable {

		System.out.println("attempting to login via Google");
		loginPage.signinwithgoogle.click();
		Log.info("Login via Google get click");

		loginPage.googleemail.sendKeys(username);
		Log.info("Enter user name");

		loginPage.NextButton.click();
		Log.info("Next button gets click");
		
		loginPage.googlepasssword.sendKeys(password);
		Log.info("Enter password");
		loginPage.googlepasssword.sendKeys(Keys.ENTER);
		
	
	
		
		Reporter.log("SignIn Action is successfully performed");
	}
	
	

}
