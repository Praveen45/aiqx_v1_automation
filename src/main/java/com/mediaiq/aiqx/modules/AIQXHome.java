package com.mediaiq.aiqx.modules;

import junit.framework.Assert;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mediaiq.aiqx.helpers.BaseClass;
import com.mediaiq.aiqx.pageobjects.AIQXHomePageObject;
import com.mediaiq.aiqx.pageobjects.LoginPageObject;
import com.mediaiq.aiqx.step_definitions.Hooks;
import com.mediaiq.aiqx.step_definitions.TestStepsAIQXHome;

@SuppressWarnings("deprecation")
public class AIQXHome extends BaseClass {
	public static AIQXHomePageObject AIQXHome_Object = PageFactory
			.initElements(Hooks.driver, AIQXHomePageObject.class);
	public static LoginPageObject loginPage = PageFactory.initElements(
			Hooks.driver, LoginPageObject.class);
	static WebDriverWait wait = new WebDriverWait(driver, 10);

	public AIQXHome(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public static void i_verify_the_aiqx_landing_discover_title_and_logged_username_as(
			String username) throws Throwable {
		String ExpectedAIQXLandingPageTitle = "DISCOVER";
		Assert.assertEquals(ExpectedAIQXLandingPageTitle,
				AIQXHome_Object.AIQXLandingPage_DiscoverTitle.getText());
		System.out.println(AIQXHome_Object.AIQXLandingPage_DiscoverTitle
				.getText());
		String s = username.substring(0, 7);
		Assert.assertEquals(AIQXHome_Object.LoggedUserName.getText(), s);
		System.out.println(AIQXHome_Object.LoggedUserName.getText());

	}

	public static void aiqx_landing_discover_title_and_logged_username_as(
			String username) throws Throwable {

		String ExpectedAIQXLandingPageTitle = "DISCOVER";
		Assert.assertEquals(ExpectedAIQXLandingPageTitle,
				AIQXHome_Object.AIQXLandingPage_DiscoverTitle.getText());
		System.out.println(AIQXHome_Object.AIQXLandingPage_DiscoverTitle
				.getText());
		String s = username.substring(0, 7);
		Assert.assertEquals(AIQXHome_Object.LoggedUserName.getText(), s);
		System.out.println(AIQXHome_Object.LoggedUserName.getText());

	}

	public static void click_on_Logout() throws Throwable {
		loginPage.UserLogoutOption.click();
		loginPage.LogoutButton.click();
		Assert.assertTrue(loginPage.aiqxwelcomemessage.isDisplayed());

	}

	// AIQX Landing Page Validation

	public static void i_enter_into_the_homepage_of_aiqx_application_with_valid_and(
			String username, String password) throws Throwable {
		AIQXLogin.i_navigate_to_the_aiqx_URL();

		AIQXLogin
				.i_enter_the_username_as_and_password_as_and_click_on_sign_in_button(
						username, password);

	}

	public static void i_verify_that_search_box_is_auto_focus()
			throws Throwable {
		Assert.assertTrue((driver.switchTo().activeElement()
				.equals(AIQXHome_Object.SearchBox)));
	}

	public static void i_check_the_menu_icon_bar_is_clickable()
			throws Throwable {
		AIQXHome_Object.AIQMenuBar.click();
		wait.until(ExpectedConditions
				.visibilityOf(AIQXHome_Object.AIQXDiscoverOption));

	}

	public static void i_verify_the_menu_list_items() throws Throwable {
		Assert.assertTrue(AIQXHome_Object.AIQXDiscoverOption.getText().equals(
				"DISCOVER"));
		Assert.assertTrue(AIQXHome_Object.AIQXPlanOption.getText().equals(
				"PLAN"));
		Assert.assertTrue(AIQXHome_Object.AIQXActivateOption.getText().equals(
				"ACTIVATE"));
		Assert.assertTrue(AIQXHome_Object.AIQXSettingOption.getText().equals(
				"SETTINGS"));
		AIQXHome_Object.AIQXMenuBarCloseButton.click();

	}

	// Report Landing screen

	public static void i_enter_the_in_search_box_of_discover_page(
			String brandname) throws Throwable {
		System.out.println("brand name : " + TestStepsAIQXHome.brandName());
		AIQXHome_Object.SearchBox.sendKeys(TestStepsAIQXHome.brandName());
		Thread.sleep(1000);

	}

	public static void i_click_on_submit_button() throws Throwable {
		AIQXHome_Object.SubmitButton.click();
	}

	public static void loading_icon_should_appear_on_the_screen()
			throws Throwable {
		Assert.assertTrue(AIQXHome_Object.LoadingIcon.isDisplayed());
	}

	public static void i_should_able_to_see_the_report_landing_page_with_query_text_as(
			String brandname) throws Throwable {
		Assert.assertEquals(AIQXHome_Object.SearchQueryText.getText(),
				brandname);
	}

	public static void compare_company_text_box_should_be_enable()
			throws Throwable {
		Assert.assertTrue(AIQXHome_Object.CompareCompanyBox.isEnabled());

	}

	public static void search_bar_should_not_have_auto_focus_in_report_landing_screen()
			throws Throwable {
		Assert.assertFalse((driver.switchTo().activeElement()
				.equals(AIQXHome_Object.SearchBox)));
	}

	// Cross Device: Conversion Path Report

	public static void i_click_on_cross_device_converting_path_report_for_the_searched(
			String brandname) throws Throwable {
		brandname = brandname.toLowerCase();
		wait.until(ExpectedConditions.visibilityOf(AIQXHome_Object
				.crossDevicePath(brandname)));
		AIQXHome_Object.crossDevicePath(brandname).click();
		sendPost();

	}

	public static void i_verify_the_report_name() throws Throwable {
		Assert.assertEquals("Cross Device: Top Converting Paths",
				AIQXHome_Object.CrossDevicePathReportTitle.getText());
	}

	public static void i_click_on_information_icon_and_verify_the_icon() throws Throwable {
		Assert.assertTrue(AIQXHome_Object.CrossDevicePathInformationIcon.isDisplayed());
		Thread.sleep(2000);
		
	}

	public static void i_verify_the_DownloadReport_and_Add_to_RFP_tabs_are_enbale() throws Throwable {
		//Assert.assertTrue(AIQXHome_Object.DownloadReportButton.isDisplayed());
		Assert.assertTrue(AIQXHome_Object.DownloadReportButton.isEnabled());
		//Assert.assertTrue(AIQXHome_Object.AddToRFPButton.isDisplayed());
		Assert.assertTrue(AIQXHome_Object.AddToRFPButton.isEnabled());
	}

	public static void i_make_a_redshift_connection() throws Throwable {
		System.out.println("Will Do ");
	}

	public static void i_validate_the_report_query_and_verify_the_same_in_ui()
			throws Throwable {
		System.out.println("Will Do Man ");
	}

}
