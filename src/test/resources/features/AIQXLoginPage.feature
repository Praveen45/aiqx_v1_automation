Feature: AIQX Login flow

  @Start
  Scenario Outline: DISCOVER Title should appear in the UI upon valid user login.
    Given I navigate to the aiqx URL
    And I enter the Username as <username> and  Password as <password> and click on Sign in button
    Then I verify the aiqx Landing Discover title and logged username as <username>
    Then Click on Logout

    Examples: 
      | username                   | password    |
      | praveen@mediaiqdigital.com | aiqxCIA2016 |

  Scenario Outline: DISCOVER Title should appear in the UI upon login with Google.
    Given I navigate to the aiqx URL
    And Enter the  <username> and  <password> and click on Sign in button
    Then Aiqx Landing Discover title and logged username as <username>
    Then Click on Logout

    Examples: 
      | username                   | password    |
      | praveen@mediaiqdigital.com | Praveen1605@ |
