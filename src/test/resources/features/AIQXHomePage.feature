Feature: AIQX Landing Page Validation

  @Start
  Scenario Outline: AIQX Homepage Validation
    Given I enter into the homepage of AIQX application with valid <username> and <password>
    And I verify that search box is auto focus
    And I check the menu icon bar is clickable
    And I verify the menu list items

    Examples: 
      | username                   | password    |
      | praveen@mediaiqdigital.com | aiqxCIA2016 |

  Scenario Outline: Search for any brands
    Given I enter the <BrandName> in search box of discover page
    And I click on submit button
    And Loading icon should appear on the screen
    Then I should able to see the Report landing page with query text as <BrandName>
    And Compare Company  text box should be enable
    And Search bar should not have auto focus in report landing screen

    Examples: 
      | BrandName |
      | SAMSUNG   |

 
  Scenario Outline: Validation on Cross Device: Top Converting Paths
    
    And I click on Cross Device converting path report for the searched <BrandName>
    And I verify the report name
    And  I click on information icon and verify the icon
    And I verify the DownloadReport and Add to RFP tabs are enbale
    
    Examples: 
      | BrandName |
      | SAMSUNG   |
    
    
  #   @SetupRedShiftDB
  #   Scenario Outline: Validate the Cross Device Report with Database
  #   
  #  Then I make a redshift connection
  #  And I validate the report query and verify the same in UI

    