package com.mediaiq.aiqx.test;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = { "classpath:features/AIQXHomePage.feature" },
glue = "classpath:com.mediaiq.aiqx.step_definitions", plugin = {
		"pretty", "html:target/cucumber-html-report",
		"json:target/Cucumber.json" }, tags = {})

public class RunAIQXHomeTest {

}
